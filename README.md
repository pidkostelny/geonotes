# GeoNotes
The GeoNotes mobile app enables you to choose the most convenient way to set up a useful schedule.
Use the geofencing function to create a notification for specific locations you are entering or leaving. Even with your device inactive, the app will automatically remind you to take action whenever it tracks a certain location.
<a href='https://play.google.com/store/apps/details?id=co.geonotes&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>