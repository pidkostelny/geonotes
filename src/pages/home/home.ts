import {Component, OnInit} from '@angular/core';

import {
  ActionSheetController, Ion, NavController, Platform, Popover, PopoverController
} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";

import {NotePage} from "../note/note";
import {NoteService} from "../../shared/services/note.service";
import {Note} from "../../shared/classes/Note";
import {ListOptionsPopover} from "./list-options.popover";
import {ItemOptionsPopover} from "./item-options.popover";
import {GeoNotePage} from "../geo-note/geo-note";
import {GeoNote} from "../../shared/classes/GeoNote";
import {GeoNotesService} from "../../shared/services/geo-notes.service";
import {BackgroundMode} from "@ionic-native/background-mode";
import {Storage} from "@ionic/storage";
import {NotificationPage} from "../notification/notification";
import {Network} from "@ionic-native/network";
import {AdService} from "../../shared/services/ad.service";
import {Geolocation} from "@ionic-native/geolocation";
import {AndroidPermissions} from "@ionic-native/android-permissions";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  notes: any[] = [];

  selected = 0;
  isEnableSelectingMode: boolean = false;
  popover: Popover;

  itemOptionsPopover: Popover;

  constructor(public navCtrl: NavController,
              public actionSheetCtrl: ActionSheetController,
              public platform: Platform,
              public noteService: NoteService,
              public geoService: GeoNotesService,
              public translate: TranslateService,
              public popoverCtrl: PopoverController,
              public bgMode: BackgroundMode,
              public storage: Storage,
              public geo: Geolocation,
              public adService: AdService,
              public permission: AndroidPermissions) {}

  ngOnInit() {
    this.initNotes();
  }

  openItemOptionsPopover(note, noteElement: Ion) {
    let dataToPopover = {
      note: note,
      noteElement: noteElement._elementRef.nativeElement
    };
    this.itemOptionsPopover = this.popoverCtrl.create(ItemOptionsPopover, dataToPopover);

    this.itemOptionsPopover.onDidDismiss(isOpenNotificationPage => {
      if (isOpenNotificationPage)
        this.navCtrl.push(NotificationPage, {note: note})
    });

    this.itemOptionsPopover.present();
  }

  openListOptionsPopover(e) {
    this.popover = this.popoverCtrl.create(ListOptionsPopover);
    this.popover.onDidDismiss(data => {
      data ? this.isEnableSelectingMode = data.isEnableSelectingMode : this.isEnableSelectingMode = false;
    });
    this.popover.present({ev:e});
  }

  closeListOptionsPopover() {
    this.isEnableSelectingMode = false;
  }

  deleteSelectedItems() {
    let selectedNotes = [],
      selectedGeoNotes = [];
    this.notes.forEach(note => {
      if (note.selected) {
        note.content.lat ? selectedGeoNotes.push(note) : selectedNotes.push(note);
      }
    });
    this.noteService.removeArray(selectedNotes).then(() => {
      this.geoService.removeArray(selectedGeoNotes).then(() => {
        this.initNotes();
      });
    });
    this.isEnableSelectingMode = false;
  }

  selectItem(e, note) {
    e.stopPropagation();
    note.selected = !note.selected;
    this.selected = this.notes.filter(e => e.selected).length;
  }

  openNote(note) {
    if (note.content.lat){
      this.navCtrl.push(GeoNotePage,{"note" : note});
    } else {
      this.navCtrl.push(NotePage, {"note": note});
    }
  }

  private initNotes() {
    this.noteService.getAll()
      .then(notes => this.notes = notes)
      .then(() => {
        this.geoService.getAll().then(geos => {
          geos.forEach(v => this.notes.push(v));
        });
      })
      .then(() => {
        this.notes.sort((v1, v2) => {
          return (v1.refreshed || v1.created).getTime() - (v2.refreshed || v2.created).getTime();
        })
      })
      .catch();
  }

  openMenu() {
    this.adService.prepareBanner();

    let translate = this.actionSheetTranslation();

    let actionSheet = this.actionSheetCtrl.create({
      title: translate.create,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: translate.note,
          icon: !this.platform.is('ios') ? 'document' : null,
          handler: () => {
            this.navCtrl.push(NotePage, {"note" : new Note()});
          }
        },
        {
          text: translate.geoNote,
          icon: !this.platform.is('ios') ? 'navigate' : null,
          handler: () => {
            this.getPermissionsForGeolocation().then(hasPermission => {
              this.navCtrl.push(GeoNotePage,{"note" : new GeoNote(), "hasPermission": hasPermission});
            });
          }
        },
        {
          text: translate.cancel,
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
        }
      ]
    });
    actionSheet.present();
  }

  private actionSheetTranslation() {
    let translation = {
      create: "Create",
      cancel: "Cancel",
      note: "Note",
      geoNote: "GeoNote",
    };

    this.translate.get('CREATE').subscribe(val => translation.create = val);
    this.translate.get('CANCEL').subscribe(val => translation.cancel = val);
    this.translate.get('NOTE').subscribe(val => translation.note = val);
    this.translate.get('GEO_NOTE').subscribe(val => translation.geoNote = val);

    return translation;
  }

  private getPermissionsForGeolocation() {


    let locationPermissions = [this.permission.PERMISSION.ACCESS_FINE_LOCATION, this.permission.PERMISSION.ACCESS_COARSE_LOCATION];
    return this.permission.hasPermission(locationPermissions[0])
      .then((val) => {
        if (!val.hasPermission) {
          return this.permission.requestPermissions(locationPermissions)
            .then(val => {
              return val;
            })
            .catch(err => alert(JSON.stringify(err)));
        } else {
          return true;
        }
      })
  }
}
