import {Component} from "@angular/core";
import {ViewController} from "ionic-angular";

@Component({
  template: `    
    <button ion-button color="dark" (click)="enableSelectingMode()" clear full>{{'SELECT' | translate}}</button>
  `
})
export class ListOptionsPopover {
  constructor(public viewCtrl: ViewController) {}

  enableSelectingMode() {
    this.viewCtrl.dismiss({isEnableSelectingMode: true});
  }
}
