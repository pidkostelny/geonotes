import {Component} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";

@Component({
  template: `
    <ion-header>
      <ion-toolbar>
        <ion-title>
          Description
        </ion-title>
        <ion-buttons start>
          <button ion-button (click)="dismiss()">
            <span ion-text color="primary" showWhen="ios">Cancel</span>
            <ion-icon name="md-close" showWhen="android,windows"></ion-icon>
          </button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>

    <ion-content padding>
      <ion-textarea [(ngModel)]="noteLocation" clearInput></ion-textarea>
      <ion-textarea [(ngModel)]="noteDescription" placeholder="{{'ENTER_DESCRIPTION' | translate}}" clearInput></ion-textarea>
    </ion-content>`
})
export class GeoNoteDescriptionModal {

  noteLocation: string = "";

  noteDescription: string = "";

  constructor(private params: NavParams,
              private viewCtrl: ViewController) {
    this.noteLocation = params.get('location');
    this.noteDescription = params.get('description');
  }

  dismiss() {
    this.viewCtrl.dismiss({description: this.noteDescription, location: this.noteLocation});
  }
}
