import {AfterContentChecked, AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  Circle, LatLng
} from '@ionic-native/google-maps';

import {NavController, ModalController, NavParams, LoadingController} from 'ionic-angular';
import {GeoNote} from "../../shared/classes/GeoNote";
import {GeocoderService} from "../../shared/services/geocoder.service";
import {DefaultOptions} from "../../shared/default-map-options";
import {GeoNoteDescriptionModal} from "./geo-note-description.modal";
import {GeoNotesService} from "../../shared/services/geo-notes.service";
import {GeofenceService} from "../../shared/services/geofence.service";
import {AdService} from "../../shared/services/ad.service";
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'page-geo-note',
  templateUrl: 'geo-note.html'
})
export class GeoNotePage implements OnInit, AfterContentInit, OnDestroy {

  map: GoogleMap;
  mapElement: HTMLElement;
  marker: Marker;
  circle: Circle;
  radius = 350;

  hasPermission = false;

  isPointExist: boolean = false;

  note: GeoNote;

  selectRadiusElement;

  loading;

  constructor(public navCtrl: NavController,
              private googleMaps: GoogleMaps,
              private params: NavParams,
              private modalCtrl: ModalController,
              private service: GeoNotesService,
              private geocoder: GeocoderService,
              private geofence: GeofenceService,
              private opt: DefaultOptions,
              private loadingCtrl: LoadingController,
              private adService: AdService,
              private translate: TranslateService) {}

  ngOnInit() {
    this.note = this.params.get("note");
    this.hasPermission = this.params.get("hasPermission");
    this.loading = this.loadingCtrl.create();
  }

  ngAfterContentInit () {
    this.loading.present();
    this.adService.showBanner();
    this.selectRadiusElement = document.getElementById('range');
    this.initMap().then(() => {
      if (this.note.content) {
        this.map.moveCamera({target: this.note.content, zoom: 14});
        this.isPointExist = true;
        this.radius = this.note.radius;
        this.setMarker(this.note)
          .then(() => {return this.setCircle(this.note.content)})
          .then(() => this.loading.dismiss());
      } else {
        this.initPosition();
      }
    });
    this.adService.prepareInterstitial();
  }

  ngOnDestroy() {
    this.adService.hideBanner();
    this.adService.showInterstitial();
  }

  ionViewWillLeave() {
    this.adService.hideBanner();
    this.adService.showInterstitial();
  }

  initMap() {

    this.mapElement = document.getElementById('map');

    this.map = this.googleMaps.create(this.mapElement, this.opt.getMapOptions());

    return this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(latLng => {
        return this.setPoint(latLng[0]);
      });
    });
  }

  private initPosition() {
    if (this.hasPermission) {
      return this.map.getMyLocation()
        .then(pos => {
          this.loading.dismiss();
          this.map.moveCamera({
            target: pos.latLng,
            zoom: 14
          })
        })
        .catch(err => {
          this.loading.dismiss();
          // err.status = false
          // err.error_code = "service_denide"
        });
    } else {
      let dismissPermissionMessage = "Because you denied access to geolocation you cannot get the notification when you will be in the radius and some other features will be unreached";
      this.translate.get("DISMISS_LOCATION_PERMISSION").subscribe(val => dismissPermissionMessage = val);
      alert(dismissPermissionMessage);
    }
  }

  private setPoint(latLng) {
    let loading = this.loadingCtrl.create();
    loading.present();
    return this.setPlaceAndMarker(latLng)
      .then(() => {return this.setCircle(latLng)})
      .then(() => loading.dismiss());
  }

  private setPlaceAndMarker(latLng) {
    if (this.isPointExist) this.removePoint();
    this.isPointExist = true;
    return this.geocoder.getPlace(latLng).then((location) => {
      this.note.content = latLng;
      this.note.description = location || "-";
      return this.map.addMarker(this.opt.getMarkerOptions(latLng, location, this.note.title)).then(marker => {
        this.marker = marker;
        marker.showInfoWindow();
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.selectRadiusElement.style = 'display:none';
        });
        marker.on(GoogleMapsEvent.INFO_CLICK).subscribe(() => {
          this.createModal({location: this.note.description, description: this.note.title || ""})
        });
      });
    });
  }

  private setMarker(note: GeoNote) {
    return this.map.addMarker(this.opt.getMarkerOptions(note.content, note.description, note.title)).then(marker => {
      this.marker = marker;
      marker.showInfoWindow();
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.selectRadiusElement.style = 'display:none';
      });
      marker.on(GoogleMapsEvent.INFO_CLICK).subscribe(() => {
        this.createModal({location: this.note.description, description: this.note.title})
      });
    });
  }

  private setCircle(latLng) {
    return this.map.addCircle(this.opt.getCircleOptions(latLng, this.radius)).then(circle => {
      this.circle = circle;
      this.selectRadiusElement.style = '';
      circle.one(GoogleMapsEvent.CIRCLE_CLICK).then(latLng => {
        this.setPoint(latLng[0]);
      });
      this.marker.on(GoogleMapsEvent.MARKER_DRAG).subscribe(latLng => {
        this.circle.setCenter(latLng[0]);
      });
    });
  }

  private removePoint() {
    this.circle.remove();
    this.marker.remove();
    this.marker = null;
    this.circle = null;
    this.isPointExist = false;
  }

  changeRadius() {
    this.circle.setRadius(this.radius);
  }

  createModal(info) {
    let data = info || {location: "", description: ""};
    let modal = this.modalCtrl.create(GeoNoteDescriptionModal, data);
    modal.onDidDismiss(data =>  {
      this.marker.hideInfoWindow();
      this.marker.setTitle(data.description);
      this.note.title = data.description;
      this.note.description = data.location;
      this.marker.showInfoWindow();
    });
    modal.present();
  }

  saveNote() {
    this.note.radius = this.radius;
    (this.note.id ? this.service.update(this.note) : this.service.create(this.note)).then(() => {
      this.geofence.addGeofence(this.note);
      this.adService.hideBanner();
      this.navCtrl.goToRoot({}).then(() => this.adService.showInterstitial());
    });
  }
}




