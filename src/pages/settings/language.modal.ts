import {Component} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";
import {TranslateService} from "@ngx-translate/core";

@Component({
  template: `
    <ion-header>
      <ion-toolbar>
        <ion-buttons start>
          <button ion-button (click)="dismiss()">
            <span ion-text color="primary" showWhen="ios">Cancel</span>
            <ion-icon name="md-close" showWhen="android,windows"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>
          {{'LANGUAGE' | translate}}
        </ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button ion-item *ngFor="let lang of languages" (click)="setLanguage(lang)">
          {{lang.name}}
        </button>
      </ion-list>
    </ion-content>
    `
})
export class LanguageModal {

  languages: { name: string, keyWord: string}[];

  constructor(private translate: TranslateService,
              private viewCtrl: ViewController) {
    this.languages = [{name:"English", keyWord: "en-US"}, { name:"Українська", keyWord : "uk-UA"}];
  }

  setLanguage(language) {
    this.translate.setDefaultLang(language.keyWord);
    this.viewCtrl.dismiss(language.keyWord);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
