import {Component, OnInit} from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';
import {LanguageModal} from "./language.modal";
import {Storage} from "@ionic/storage";
import {Settings, SETTINGS_KEY} from "../../shared/setings";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage implements OnInit {

  settings: Settings = new Settings();

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public storage: Storage) {}

  ngOnInit() {
    this.storage.get(SETTINGS_KEY).then(settings => settings && (this.settings = settings)).then(() => console.log(this.settings));
  }

  openLanguages() {
    let modal = this.modalCtrl.create(LanguageModal);
    modal.onDidDismiss(lang => {
      console.log(lang);
      lang && (this.settings.language = lang);
      console.log(this.settings);
      this.saveSettings();
    });
    modal.present();
  }

  setOnLeave(val) {
    this.settings.isEnableOnLeaveNotification = val;
    this.saveSettings();
  }

  private saveSettings() {
    this.storage.set(SETTINGS_KEY, this.settings);
  }

}
