
import {Component, ElementRef, AfterViewInit, OnInit,} from "@angular/core";

import * as Quill from 'quill';
import {TranslateService} from "@ngx-translate/core";
import {NavController, NavParams} from "ionic-angular";
import {NoteService} from "../../shared/services/note.service";
import {Note} from "../../shared/classes/Note";
import {HomePage} from "../home/home";
import {AdService} from "../../shared/services/ad.service";
import {AdMobFree} from "@ionic-native/admob-free";


@Component({
  selector: 'page-note',
  templateUrl: './note.html'
})
export class NotePage implements AfterViewInit, OnInit {

  private textEditor: Quill;

  private addBulletListElement;
  private addOrderedListElement;
  private addImageElement;
  private isClicked: boolean = false;

  private note: Note;

  constructor(private translate: TranslateService,
              private noteService: NoteService,
              private elRef: ElementRef,
              private navParams: NavParams,
              private navCtrl: NavController,
              private adService: AdService) {}

  ngOnInit() {
    this.note = this.navParams.get("note");
  }

  ngAfterViewInit() {
    let placeholder: string;
    this.translate.get('NOTE_GOES_HERE').subscribe(val => placeholder = val);
    this.textEditor = new Quill("#noteArea", {
      theme: 'bubble',
      placeholder: placeholder,
      modules: {
        history: {
          delay: 2000,
          maxStack: 50,
          userOnly: true
        },
        toolbar: [
          ['bold', 'italic', 'underline'],
          [{ 'color': [] }, { 'background': [] }],
          [{ 'align': [] }, { 'indent': '-1'}, { 'indent': '+1' }],
          [{ 'script': 'sub'}, { 'script': 'super' },
            { 'list': 'ordered'}, { 'list': 'bullet' }, 'image'],
        ]
      }
    });
    if (this.note.content) {
      this.textEditor.setContents(JSON.parse(this.note.content));
    }

    this.adService.prepareInterstitial();

    this.addOrderedListElement = this.elRef.nativeElement.querySelector('.ql-list[value=ordered]');
    this.addBulletListElement = this.elRef.nativeElement.querySelector('.ql-list[value=bullet]');
    this.addImageElement = this.elRef.nativeElement.querySelector('.ql-image');
  }

  addList() {
    this.addBulletListElement.click();
    // if(!this.isClicked && !this.elRef.nativeElement.querySelector('ol li')) {
    //   this.addBulletListElement.click();
    //   this.isClicked = true;
    // } else {
    //   this.addOrderedListElement.click();
    // }
    // setTimeout((function () {this.isClicked = false;}).bind(this), 400);
  }

  addImage() {
    this.addImageElement.click();
  }

  saveNote() {
    this.note.content = JSON.stringify(this.textEditor.getContents());
    this.note.description = this.textEditor.getText(0, 200);
    (this.note.id ? this.noteService.update(this.note) : this.noteService.create(this.note)).then(() => {
      this.navCtrl.setRoot(HomePage).then(() => this.adService.showInterstitial());
    });
  }

}
