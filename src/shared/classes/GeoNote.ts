import { Note } from "./Note";
import {Category} from "./category";
import {LatLng} from "@ionic-native/google-maps";

export class GeoNote extends Note {

  constructor(public radius: number = 500,
              public id: string = "",
              public title: string = "") {
    super(id, title);
  }
}
