
import { Category } from './category';

export class Note {

  public reminderId = 0;
  public category = null;
  public content;
  public refreshed: Date;

  constructor(public id: string = "",
              public title: string = "",
              public description: string = "",
              public created: Date = new Date(),
              public isNotificationEnable: boolean = false,
              public selected: boolean = false) {}
}
