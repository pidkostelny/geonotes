import {CircleOptions, GoogleMapOptions, MarkerOptions} from "@ionic-native/google-maps";
import {Injectable} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class DefaultOptions {

  constructor(private translate: TranslateService){}

  getMapOptions(): GoogleMapOptions {
    return {
      controls: {
        compass: true,
        myLocationButton: true,
      },
      styles: [{
        featureType: "all",
        stylers: [{ saturation: -10 }]
      }]
    }
  }

  getCircleOptions(latLng, radius): CircleOptions {
    return {
      center: latLng,
      radius: radius,
      strokeColor: "#999999",
      strokeWidth: 3,
      fillColor: "#999999",
    }
  }

  getMarkerOptions(latLng, location, description): MarkerOptions {
    if (!description) {
      this.translate.get("DEFAULT_GEO_DESCRIPTION").subscribe(val => description = val);
    }
    return {
      title: description || "Tape to enter the description",
      snippet:  location,
      animation: 'DROP',
      draggable: true,
      position: latLng,
    }
  }
}


