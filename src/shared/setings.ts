export const SETTINGS_KEY = "_settings";

export class Settings {
  language: string = "en-US";
  isEnableOnLeaveNotification = false;
  isEnableSoundNotification = false;
  ledColor = "38B2CE";
}
