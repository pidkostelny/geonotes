import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

const MS_IN_MONTH = 2592000000;
const MS_IN_WEEK  = 604800000;
const MS_IN_DAY   = 86400000;

@Pipe({
  name: 'localizedDate',
  pure: false
})
export class LocalizedDatePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: string, pattern): any {
    const datePipe: DatePipe = new DatePipe(this.translateService.getDefaultLang());
    let timeIntervalBetweenTodayCreation = new Date().getTime() - Date.parse(value);
    if(MS_IN_DAY > timeIntervalBetweenTodayCreation) {
      pattern = 'jm';
    } else if(MS_IN_WEEK > timeIntervalBetweenTodayCreation) {
      pattern = 'EEEE';
    } else if(MS_IN_MONTH > timeIntervalBetweenTodayCreation) {
      pattern = 'd MMM';
    }
    return datePipe.transform(value, pattern);
  }

}
