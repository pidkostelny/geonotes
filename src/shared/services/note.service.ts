
import { Injectable, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

import generateUUID from "../utils/uuid";
import 'rxjs/add/operator/toPromise';
import {Note} from "../classes/Note";



const NOTES_STORAGE_KEY = 'notes';

@Injectable()
export class NoteService implements OnInit {

  constructor(private storage: Storage) { }

  ngOnInit() {
    this.storage.ready();
  }

  getAll(): Promise<Note[]> {
    return this.storage.get(NOTES_STORAGE_KEY).then(notes => {
      if (notes) {
        return notes;
      } else {
        return this.storage.set(NOTES_STORAGE_KEY, []);
      }
    });
  }

  getOne(id) {
    return this.getAll().then(notes => {
      return notes.find(v => v.id === id);
    })
  }

  create(note: Note): Promise<Note> {
    let id = generateUUID();
    return this.getAll().then((notes: Note[]) => {
      while (!notes.every(v => {return v.id !== id})) {
        id = generateUUID();
      }
      note.id = id;
      note.created = new Date();
      notes.push(note);
      this.storage.set(NOTES_STORAGE_KEY, notes);
      return note;
    });
  }

  remove(note: Note): Promise<boolean> {
    return this.getAll().then((notes: Note[]) => {
      for (let i = 0; i < notes.length; i++) {
        if (notes[i].id === note.id) {
          notes.splice(i, 1);
          return this.storage.set(NOTES_STORAGE_KEY, notes).then(() => {return true;});
        }
      }
      return false;
    });
  }

  removeArray(array: Note[]): Promise<boolean> {
    return this.getAll().then((notes: Note[]) => {
      for (let i = 0; i < notes.length; i++) {
        for (let j = 0; j < array.length; j++) {
          if (notes[i].id === array[j].id) {
            notes.splice(i,1);
          }
        }
      }
      return this.storage.set(NOTES_STORAGE_KEY, notes).then(() => {return true;});
    });
  }

  update(note: Note): Promise<Note> {
    return this.getAll().then((notes: Note[]) => {
      notes.some((value, index) => {
        if (value.id === note.id) {
          note.refreshed = new Date();
          notes[index] = note;
          return true;
        } else {
          return false;
        }
      });
      return this.storage.set(NOTES_STORAGE_KEY, notes).then(() => {
        return note;
      });
    });
  }
}
