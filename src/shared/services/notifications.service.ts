
import {Injectable} from "@angular/core";
import {ILocalNotification, LocalNotifications} from "@ionic-native/local-notifications";
import {Storage} from "@ionic/storage";
import {Note} from "../classes/Note";
import {SETTINGS_KEY} from "../setings";
import {NoteService} from "./note.service";

export const NOTIFICATION_KEY = "_notification";

@Injectable()
export class NotificationsService {

  constructor(private notifications: LocalNotifications,
              private storage: Storage,
              private noteService: NoteService) {}

  setNotification(note: Note, notificationOptions) {
    return this.checkPermission().then(() => {
      return this.getId().then(id => {
        let options: ILocalNotification = {
          id: id,
          title: note.title,
          text: note.description,
          firstAt:  notificationOptions.date,
          data: note.id,
          led: "38B2CE"
        };

        this.notifications.on('trigger', notification => {
          this.noteService.getOne(notification.data).then(note => {
            note.reminderId = 0;
            this.noteService.update(note);
          })
        });

        return this.storage.get(SETTINGS_KEY)
          .then(set => {
            options.led = set.ledColor;
          })
          .then(() => this.notifications.schedule(options))
          .then(() => this.storage.set(NOTIFICATION_KEY, id + 1))
          .then(() => { return id });
      });
    })
  }

  cancelNotification(id) {
    return this.notifications.cancel(id);
  }

  private getId() {
    return this.storage.get(NOTIFICATION_KEY).then(val => {return val});
  }

  private checkPermission() {
    return this.notifications.hasPermission().then(hasPermission => {
      if (!hasPermission) {
        return this.notifications.registerPermission().then(val => {return val});
      }
    })
  }
}
