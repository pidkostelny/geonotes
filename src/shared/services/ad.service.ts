
import {Injectable, OnInit} from "@angular/core";
import {AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig} from "@ionic-native/admob-free";
import {Network} from "@ionic-native/network";

const bannerConfig: AdMobFreeBannerConfig = {
  isTesting: true,
  autoShow: false,
  id: "ca-app-pub-3814433054846797/1401994744"
};

const interstitialConfig: AdMobFreeInterstitialConfig = {
  id: "ca-app-pub-3814433054846797/9914376751",
  isTesting: true,
  autoShow: false
};

@Injectable()
export class AdService {

  public bannerPrepared = false;
  public interstitialPrepared = false;

  constructor(private admob: AdMobFree,
              private network: Network) {}

  prepareBanner() {
    this.admob.banner.config(bannerConfig);

    return this.admob.banner.prepare().then(() => {
      this.bannerPrepared = true;
    });
  }


  showBanner() {
    if (this.network.type == 'none') return;

    if (this.bannerPrepared) {
      this.admob.banner.show();
    } else {
      this.prepareBanner().then(() => this.admob.banner.show());
    }
  }

  hideBanner() {
    this.admob.banner.hide();
  }

  prepareInterstitial() {
    this.admob.interstitial.config(interstitialConfig);

    return this.admob.interstitial.prepare().then(() => {
      this.interstitialPrepared = true;
    });
  }

  showInterstitial() {
    if (this.network.type == 'none') return;

    if (this.interstitialPrepared) {
      this.admob.interstitial.show();
    } else {
      this.prepareInterstitial().then(() => this.admob.interstitial.show());
    }
  }

}
