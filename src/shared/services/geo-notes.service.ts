
import { Injectable, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/toPromise';
import {GeoNote} from "../classes/GeoNote";
import generateUUID from "../utils/uuid";

const GEO_STORAGE_KEY = 'geo-notes';

@Injectable()
export class GeoNotesService implements OnInit {

  constructor(private storage: Storage) { }

  ngOnInit() {
    this.storage.ready();
  }

  getAll(): Promise<GeoNote[]> {
    return this.storage.get(GEO_STORAGE_KEY).then(notes => {
      if (notes) {
        return notes;
      } else {
        return this.storage.set(GEO_STORAGE_KEY, []);
      }
    });
  }

  create(note: GeoNote): Promise<GeoNote> {
    let id = generateUUID();
    return this.getAll().then((notes: GeoNote[]) => {
      while (!notes.every(v => {return v.id !== id})) {
        id = generateUUID();
      }
      note.id = id;
      note.created = new Date();
      notes.push(note);
      return this.storage.set(GEO_STORAGE_KEY, notes).then(() => {
        return note;
      });
    });
  }

  remove(note: GeoNote): Promise<boolean> {
    return this.getAll().then((notes: GeoNote[]) => {
      for (let i = 0; i < notes.length; i++) {
        if (notes[i].id === note.id) {
          notes.splice(i, 1);
          return this.storage.set(GEO_STORAGE_KEY, notes).then(() => {return true;});
        }
      }
      return false;
    });
  }

  removeArray(array: GeoNote[]): Promise<boolean> {
    return this.getAll().then((notes: GeoNote[]) => {
      for (let i = 0; i < notes.length; i++) {
        for (let j = 0; j < array.length; j++) {
          if (notes[i].id === array[j].id) {
            notes.splice(i,1);
          }
        }
      }
      return this.storage.set(GEO_STORAGE_KEY, notes).then(() => {return true;});
    });
  }

  findOne(noteId: String) {
    return this.getAll().then(notes => {
      return notes.find(e => { return e.id === noteId });
    })
  }

  update(note: GeoNote): Promise<GeoNote> {
    return this.getAll().then((notes: GeoNote[]) => {
      notes.every((value, index) => {
        if (value.id === note.id) {
          note.refreshed = new Date();
          notes[index] = note;
          return true;
        } else {
          return false;
        }
      });
      return this.storage.set(GEO_STORAGE_KEY, notes).then(() => { return note });
    });
  }
}
