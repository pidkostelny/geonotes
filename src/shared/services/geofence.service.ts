import {Geofence} from "@ionic-native/geofence";
import {Injectable, OnInit} from "@angular/core";
import { Storage } from '@ionic/storage';
import {GeoNote} from "../classes/GeoNote";
import {SETTINGS_KEY} from "../setings";
import {App, NavController, Platform} from "ionic-angular";


@Injectable()
export class GeofenceService implements OnInit {

  navCtrl: NavController;

  constructor(public platform: Platform,
              private geofence: Geofence,
              private storage: Storage,
              private app: App){
    this.navCtrl = app.getRootNav();
  }

  ngOnInit() {
    this.geofence.initialize();
  }

  public addGeofence(note: GeoNote) {
    let fence = {
      id:         note.id,
      latitude:   note.content.lat,
      longitude:  note.content.lng,
      radius:     note.radius || 400,
      transitionType: 1,
      notification: {
        title:          note.title,
        text:           note.description,
        vibration:      [618, 206, 618],
        openAppOnClick: true,
        data: note
      }
    };
    this.getIsOnLeaveEnable()
      .then(isOnLeaveEnable => fence.transitionType = isOnLeaveEnable ? 3 : 1)
      .then(() => {
        this.geofence.addOrUpdate(fence);
    });
  }

  remove(id: string | string[]) {
    this.geofence.remove(id)
      .catch();
  }

  private getIsOnLeaveEnable() {
    return this.storage.get(SETTINGS_KEY)
      .then(settings => {return settings.isEnableOnLeaveNotification});
  }

}
