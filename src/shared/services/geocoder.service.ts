
import {Geocoder} from "@ionic-native/google-maps";
import {Injectable} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class GeocoderService {

  geocoder = new Geocoder();
  constructor(private translate: TranslateService) {}

  getPlace(latLng): Promise<string> {
    return this.geocoder.geocode({position: latLng})
      .then(res => {
        if (res[0]) {
          return this.geocoderResponseToString(res);
        } else {
          let text = "There is no information";
          this.translate.get("NO_GEO_DATA").subscribe(val => {
            text = val;
          });
          return Promise.resolve(text);
        }
      })
      .catch(err => {

      });
  }

  getPosition(place: string) {
    return this.geocoder.geocode({address: place}).then(res => {
    });
  }

  private geocoderResponseToString(res): string {
    let arr = [];
    res[0].subThoroughfare && arr.push(res[0].subThoroughfare);
    res[0].thoroughfare && arr.push(res[0].thoroughfare);
    res[0].locality && arr.push(res[0].locality);
    res[0].adminArea && arr.push(res[0].adminArea);
    res[0].postalCode && arr.push(res[0].postalCode);
    res[0].country && arr.push(res[0].country);
    return arr.join(', ');
  }

}
