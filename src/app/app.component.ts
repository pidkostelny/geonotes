import {Component, OnInit, ViewChild} from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../pages/home/home';

import { langs } from "../assets/i18n/langs";
import {Settings, SETTINGS_KEY} from "../shared/setings";
import {SettingsPage} from "../pages/settings/setting";
import {Storage} from "@ionic/storage";
import {NOTIFICATION_KEY} from "../shared/services/notifications.service";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  settings: Settings = new Settings();

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public storage: Storage,
              public translate: TranslateService) {}

  ngOnInit() {
    this.initializeApp();
  }

  initializeApp() {
    return this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.initStorage().then(() => {
        this.initTranslate();
      });
      this.splashScreen.hide();
    });
  }

  initTranslate() {
    this.translate.addLangs(langs);

    let platformLang =  this.platform.lang();

    this.storage.get('IS_FIRST_RUN_COMPLETED').then(val => {
      if (!val) {
        langs.find(v => {
          if (v.match(new RegExp(platformLang, 'gi'))) {
            this.settings.language = v;
            this.storage.set(SETTINGS_KEY, this.settings);
            return true;
          }
        });
        this.storage.set('IS_FIRST_RUN_COMPLETED', true);
      }
    });

    this.translate.setDefaultLang(this.settings.language);

    this.pages = [
      { title: "HOME", component: HomePage },
      { title: "SETTINGS", component: SettingsPage }
    ];
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  initStorage() {
    return this.storage.ready()
      .then(() => {
        return this.storage.get(SETTINGS_KEY).then(val => {
          if (val) {
            this.settings = val;
          } else {
            this.storage.set(SETTINGS_KEY, new Settings());
          }
        });
    })
      .then(() => {
        return this.storage.get(NOTIFICATION_KEY).then(val => {
          if (!val) {
            val = 1;
          }
        })
      })
  }

}
