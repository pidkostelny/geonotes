import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { Http, HttpModule} from "@angular/http";
import {IonicApp, IonicErrorHandler, IonicModule, NavController} from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {NotePage} from "../pages/note/note";
import {NoteService} from "../shared/services/note.service";
import {ListOptionsPopover} from "../pages/home/list-options.popover";
import {PipesModule} from "../shared/pipes/pipes.module";
import {ItemOptionsPopover} from "../pages/home/item-options.popover";
import {LanguageModal} from "../pages/settings/language.modal";
import {SettingsPage} from "../pages/settings/setting";
import {GeoNotePage} from "../pages/geo-note/geo-note";
import {NativeGeocoder} from "@ionic-native/native-geocoder";
import {GoogleMaps} from "@ionic-native/google-maps";
import {GeocoderService} from "../shared/services/geocoder.service";
import {DefaultOptions} from "../shared/default-map-options";
import {GeoNoteDescriptionModal} from "../pages/geo-note/geo-note-description.modal";
import {GeoNotesService} from "../shared/services/geo-notes.service";
import {Geofence} from "@ionic-native/geofence";
import {GeofenceService} from "../shared/services/geofence.service";
import {BackgroundMode} from "@ionic-native/background-mode";
import {NotificationClickService} from "../shared/services/notification-click.service";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {NotificationPage} from "../pages/notification/notification";
import {NotificationsService} from "../shared/services/notifications.service";
import {AdMobFree} from "@ionic-native/admob-free";
import {AdService} from "../shared/services/ad.service";
import {Network} from "@ionic-native/network";
import {AndroidPermissions} from "@ionic-native/android-permissions";

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NotePage,
    GeoNotePage,
    SettingsPage,
    NotificationPage,
    ListOptionsPopover,
    ItemOptionsPopover,
    LanguageModal,
    GeoNoteDescriptionModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: 'GeoNotesDB'
    }),
    PipesModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    NotePage,
    GeoNotePage,
    SettingsPage,
    NotificationPage,
    GeoNoteDescriptionModal,
    ListOptionsPopover,
    ItemOptionsPopover,
    LanguageModal
  ],
  providers: [
    AndroidPermissions,
    AdMobFree,
    AdService,
    BackgroundMode,
    DefaultOptions,
    GoogleMaps,
    GeocoderService,
    GeoNotesService,
    GeofenceService,
    Geofence,
    NativeGeocoder,
    NoteService,
    Network,
    StatusBar,
    SplashScreen,
    NotificationsService,
    NotificationClickService,
    LocalNotifications,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
